package zengcode.medium.com.model;


import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Person {

    @GraphId
    private Long id;

    private String name;

    private Person() {
        // Empty constructor required as of Neo4j API 2.0.5
    };

    public Person(String name) {
        this.name = name;
    }

    /**
     * Neo4j doesn't REALLY have bi-directional relationships. It just means when querying
     * to ignore the direction of the relationship.
     * https://dzone.com/articles/modelling-data-neo4j
     */
    @Relationship(type = "FRIENDS", direction = Relationship.UNDIRECTED)
    public Set<Person> friends;

    public void friendOf(Person person) {
        if (friends == null) {
            friends = new HashSet<>();
        }
        friends.add(person);
    }

    @Relationship(type = "COUSINS", direction = Relationship.UNDIRECTED)
    public Set<Person> cousins;

    public void cousinOf(Person person){
        if (cousins == null){
            cousins = new HashSet<>();
        }
        cousins.add(person);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Person> getFriends() {
        return friends;
    }

    public Set<Person> getCousins() {
        return cousins;
    }

    public void setFriends(Set<Person> friends) {
        this.friends = friends;
    }

    public void setCousins(Set<Person> cousins){
        this.cousins = cousins;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
